from main import *
import os
import pandas as pd
import pytest

@pytest.fixture
def csv_file():
    tmp_path = 'data/generic-food.csv'
    return tmp_path


def test_load_data(csv_file):
    data = load_data(csv_file)
    
    assert 'PRICES' in data.columns
    assert 'FOOD NAME' in data.columns
    assert len(data) == 907


def test_clean_data(csv_file):
    data = load_data(csv_file)
    data = clean_data(data)
    
    assert len(data['PRICES'].isna().drop_duplicates().to_list()) == 1
    assert data['PRICES'].isna().drop_duplicates().to_list()[0] == False
    assert len(data) == 907  