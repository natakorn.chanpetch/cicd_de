import pandas as pd
# from sqlalchemy import create_engine

# Function to load the CSV
def load_data(file_name):
    data = pd.read_csv(file_name)
    return data

# Function to clean the data
def clean_data(data):
    data = data.fillna('null')
    return data

# Function to save the data to a SQL database
# def save_data(data, db_string, table_name):
#     engine = create_engine(db_string)
#     data.to_sql(table_name, engine, if_exists='replace')

# Run pipeline
# data = load_data('generic-food.csv')
# data = clean_data(data)
# print(data.head())
# save_data(data, 'sqlite:///database.db', 'my_table')